from __future__ import print_function, division
import numpy as np
import os
import matplotlib
from scipy import stats
from matplotlib import pyplot as plt


def find_nearest(arr, value):
    '''
    This function gives the content and the index in the array of the number that is closest to
    the value given.
    Args:
        param arr = 1-D numpy array
        param value = float or integer
    returns: 
        The array element closest to value and its index
    '''
    idx=(np.abs(arr-value)).argmin()
    return arr[idx], idx


def find_nearest_within(arr, value, threshold):
    '''
    This function gives the content in the array of the number that is closest to
    the value given, within threshold away from value.
    Args:
        arr: array to look into
        value: value to look for
        threshold: range of values to find the nearest value in the array
    Returns:
        the nearest value
    '''
    half_thres = threshold / 2.0
    choped_arr = arr[(arr >= value-half_thres) & (arr <= value+half_thres)]
    if len(choped_arr) == 0:
        return 0.0
    diff = np.abs(choped_arr - value)
    diff_min = min(diff)
    return np.squeeze(choped_arr[diff==diff_min])


def get_rms(arr, mainvals_arr):
    """
    This function calculates the RMS with respect to the array of main values.
    """
    diffs_arr = np.abs(arr - mainvals_arr)
    rms = np.sqrt(np.mean(np.square(diffs_arr)))
    return rms


def determine_SNR(wav_arr, flx_arr, flx_linefit):
    """
    This function determines the SNR of the given spectrum, provided that there is no slope in the spectrum.
    Args:
        wav_arr = numpy array
        flx_arr = numpy array
    Returns: 
        avg_signal = float
        rms = float
        snr = float
    """
    diffs_list = []
    j = 1
    for i in range(len(wav_arr)-1):
        diff = wav_arr[j] - wav_arr[i]
        diffs_list.append(diff)
        if j < len(wav_arr)-1:
            j += 1
    angstronms_per_pixel = sum(diffs_list)/len(diffs_list)
    avg_signal = np.mean(flx_arr)
    # determine the average noise from input file
    rms = get_rms(flx_arr, flx_linefit)
    # SNR from input file
    snr = avg_signal/rms
    return angstronms_per_pixel, avg_signal, rms, snr


def readlines_EWabsRelHbeta():
    '''This function specifically reads the lines of the file to correct for underlying absorption: 
        abs_EWs_rel_Hbeta.txt'''
    # Hydrogen
    H_lines = []
    H_EWratios = []
    # Helium
    He_lines = []
    He_EWratios = []
    text_file = os.path.abspath('abs_EWs_rel_Hbeta.txt')
    f = open(text_file, 'r')
    list_rows_of_file = f.readlines()
    f.close()
    for row in list_rows_of_file:
        # Disregard comment symbol
        if '#' not in row:
            # Split each row into columns
            line_data = row.split()
            wav = float(line_data[1])
            ewabs = float(line_data[2])
            if 'HI' in row:
                # append the element into each column in the Hydrogen cols_in_file
                H_lines.append(wav)
                H_EWratios.append(ewabs)
            if 'HeI' in row:
                # append the element into each column in the Hydrogen cols_in_file
                He_lines.append(wav)
                He_EWratios.append(ewabs)
    Hline_and_EWs = [H_lines, H_EWratios]
    Heline_and_EWs = [He_lines, He_EWratios]
    return Hline_and_EWs, Heline_and_EWs


def underlyingAbsCorr(catalog_wavelength, flux, continuum, EWabsHbeta):
    '''
    This function corrects for underlying absorption. 
    * Should be applied before reddening correction.
    * Assumes that there is NO collisional excitation; it uses the EW_abs of the H and He lines with
        respect to EW_abs(Hbeta).
    Args:
        catalog_wavelength = array or list
        flux = array or list
        continuum = array or list
        EWabsHbeta = float, typical value for HII regions lies between 2.0-4.0
    Returns:
        corr_intensities = list of corrected intensities (only the H and He lines are really corrected)
    '''
    Hline_and_EWs, Heline_and_EWs = readlines_EWabsRelHbeta()
    ### Both H and He lines are going to be used to correct for underlying absorption
    undabs_wav, undabs_EW = [], []
    for w, ew in zip(Hline_and_EWs[0], Hline_and_EWs[1]):
        undabs_wav.append(w)
        undabs_EW.append(ew)
    for w, ew in zip(Heline_and_EWs[0], Heline_and_EWs[1]):
        undabs_wav.append(w)
        undabs_EW.append(ew)
    ### Now add a 0.000 to all other observed lines
    corr_undelyingAbs_EWs = []
    for w in catalog_wavelength:
        if int(w) in undabs_wav:
            w_idx_undabs = undabs_wav.index(int(w))
            e = undabs_EW[w_idx_undabs]
        else:
            e = 0.000
        corr_undelyingAbs_EWs.append(e)
    ### Remove UNDERLYING ABSORPTION for optical lines to get "clean" Intensities
    corr_intensities = []
    for w, EWabsLine,cont,flx in zip(catalog_wavelength, corr_undelyingAbs_EWs, continuum, flux):
        I = EWabsLine * EWabsHbeta * cont + flx
        corr_intensities.append(I)
        #print (w, I)
    return corr_intensities


def sigma_clip_flux(flux_arr, sigmas_away):
    '''
    This function performs the clipping normalizing to sigma and according to the
    number of sigmas_away (i.e. the confidence level). It determines the standard
    deviation from the given array.
    Args:
        flx_arr = numpy array
        sigmas_away = float
    Returns:
        standard deviation
        sigma-clipped array
    '''
    std, _ = find_std(flux_arr)
    norm_flx = flux_arr /std
    clip_up = sigmas_away
    clip_down = (-1) * sigmas_away
    flx_clip = np.clip(norm_flx, clip_down, clip_up)
    return std, flx_clip*std


def iterate_sigma_clipdflx_usingMode(flx_arr, sigmas_away):
    '''
    This function does the iterations to converge to a standard deviation and then
    perform the sigma-clipping. If it cannot converge then it uses the flux mode as
    the standard deviation and then it clips.
    Args:
        flx_arr = numpy array
        sigmas_away = float
    Returns:
        Standard deviation (or flux mode).
        Clipped flux array.
    '''
    normalize2 = 1e-14
    temp_f = flx_arr / normalize2
    decimals = 2
    rounded_f = np.around(temp_f, decimals)
    original_flux_mode = stats.mode(rounded_f, axis=None)
    for f in rounded_f:
        if f == max(rounded_f) or f == min(rounded_f):
            #print 'Found a max or min: ', f
            f = float(original_flux_mode[0])
    flux_mode = stats.mode(rounded_f, axis=None)
    #print ('flux_mode', flux_mode)
    std = flux_mode[0] * normalize2
    #print ('sigma = ', std)
    clip_up = sigmas_away
    clip_down = (-1) * sigmas_away
    i = 1
    end_loop = False
    # Prevent dividing by zero
    if std == 0.0:
        end_loop = True
        std =  np.median(flx_arr)
        #print ('Mode is zero, used median as std:', std)
        n_arr = flx_arr / std
        flx_clip = np.clip(n_arr, clip_down, clip_up) * std
        return std, flx_clip
    # If the mode is not zero try to find standard deviation
    norm_flux = flx_arr / std
    flx_clip = np.clip(norm_flux, clip_down, clip_up) * std
    # Do iterations
    while end_loop == False:
        prev_std = std
        new_flx = flx_clip
        std, flx_clip = sigma_clip_flux(new_flx, sigmas_away)
        #print ('sigma = ', std)
        # did it converge?
        std_diff = np.fabs(prev_std - std)
        #print ('std_diff = ', std_diff)
        if std_diff == 0.0:
            end_loop = True
        # In case the function cannot converge, use mode flux as standard deviation
        elif (std_diff <= (flux_mode[0]*normalize2)) or (std == 0.0):
            end_loop = True
            std = float(flux_mode[0] * normalize2)
            #print ('Did not converge, used flux mode as std: ', std)
            n_arr = flx_arr / std
            flx_clip = np.clip(n_arr, clip_down, clip_up) * std
        #print ('std_diff', std_diff)
        i = i + 1
        # Stop the loop in case none of the other conditions are met
        if i > 1000:
            #print ('Reached maximum iterations without meeting the other conditions.')
            end_loop = True
    #print ('Number of iterations: ', i)
    return std, flx_clip


def get_spec_sigclip(object_spec, window, sigmas_away):
    '''
    This function does the sigma-clip iterations over the given spectrum dividing it
    in several windows.
    Args:
        object_spec = 2D array of wavelength and flux.
        window = integer, width of the windows (in angstroms) in which to divide the spectrum.
        sigma_away = float, the desired confidence interval (1sigma=68.26895%, 2sigma=95.44997%,
                    3sigma=99.73002%, 4sigma=99.99366%, 5sigma=99.99994%)
    Returns:
        Full sigma-clipped 2D array of wavelengths and flux.
    '''
    # First window
    window_lo = object_spec[0][0]
    window_up, _ = find_nearest(object_spec[0], window_lo+window)
    print ('INITIAL Window: ', window_lo, window_up)
    f_win = object_spec[1][(object_spec[0] <= window_up)]
    std, flx_clip = iterate_sigma_clipdflx_usingMode(f_win, sigmas_away)
    print ('sigma = ', std)
    # List all the standard deviations and averages
    std_list = []
    avges_list = []
    avg_window = sum(flx_clip) / float(len(flx_clip))
    # Add the clipped fluxes
    clipd_fluxes_list = []
    for f in flx_clip:
        clipd_fluxes_list.append(f)
        std_list.append(std)
        avges_list.append(avg_window)
    # Following windows
    end_loop = False
    while end_loop == False:
        window_lo = window_up
        wup_increment = window_up + window
        # Make sure that the last window is not tiny
        dist2end = max(object_spec[0]) - window_up
        #print ('dist2end', dist2end)
        if (wup_increment < max(object_spec[0])) and (dist2end >= window+100.0):
            window_up, _ = find_nearest(object_spec[0], wup_increment)
        else:
            end_loop = True
            window_up = max(object_spec[0])
        print ('Window: ', window_lo, window_up)
        f_win = object_spec[1][(object_spec[0] > window_lo) & (object_spec[0] <= window_up)]
        std, flx_clip = iterate_sigma_clipdflx_usingMode(f_win, sigmas_away)
        print ('sigma = ', std)
        avg_window = sum(flx_clip) / float(len(flx_clip))
        for f in flx_clip:
            clipd_fluxes_list.append(f)
            avges_list.append(avg_window)
            std_list.append(std)
    clipd_arr = np.array([object_spec[0], clipd_fluxes_list])
    std_arr = np.array([object_spec[0], std_list])
    avg_arr = np.array([object_spec[0], avges_list])
    return clipd_arr, std_arr, avg_arr


def find_chi2(observed_arr, expected_arr):
    mean = np.mean(observed_arr)
    variance_list = []
    for o in observed_arr:
        variance = (o - mean)**2
        variance_list.append(variance)
    variance = sum(variance_list) / float(len(observed_arr))
    #print ('variance =', variance)
    chi2 = []
    for o, e in zip(observed_arr, expected_arr):
        diff_squared = (o - e)**2
        chi2.append(diff_squared)
    chi_squared = sum(chi2) / variance
    return chi_squared


def find_reduced_chi2_of_polynomial(polynomialfit_arr, continuum_arr, window):
    ''' 
    Determine if the adjusted polynomial is a good enough fit to the continuum fluxes.
        1. Null hypothesis = there are equal number of points above and below the fitted polynomial (it passes through the middle of the data).
        2. We expect 50% of the points above the continuum and 50% below -- the expected value is then the standard deviation that I
        used to define the continuum range fluxes.
        3. The observed values are the polynomial fluxes fitted to the trimed flux array that I defined as the continuum.
        4. The expected values will be the mean values of the window fluxes of the continuum array.
        5. Degrees of freedom, df = 1 (in this case there are 2 outcomes, above and below the continuum, so  df = outcomes - 1).
    Args:
        polynomialfit_arr = 2D numpy array of wavelength and flux of the fit
        continuum_arr = 2D array of wavelength and sigma-cliped flux
        window = float, angstroms of the wavelength window to find continuum
    Returns:
        total_chi2 = float, summed chi2
        chi2_list = list of window chi2s
        window_cont_list = list of continuum arrays per window
        err_fit = float, percentage of error of continuum fit
    '''
    # First window
    window_lo = polynomialfit_arr[0][0]
    window_up, _ = find_nearest(polynomialfit_arr[0], window_lo+window)
    #print ('INITIAL Window: ', window_lo, window_up)
    pol_wavs_lists = []
    pol_flxs_lists = []
    w_win = polynomialfit_arr[0][(polynomialfit_arr[0] <= window_up)]
    f_win = polynomialfit_arr[1][(polynomialfit_arr[0] <= window_up)]
    pol_wavs_lists.append(w_win)
    pol_flxs_lists.append(f_win)
    # Define per window the expected_value_arr as the median of the trimmed flux that I defined as continuum_arr
    window_cont_list = []
    w_cont = continuum_arr[0][(continuum_arr[0] <= window_up)]
    f_cont = continuum_arr[1][(continuum_arr[0] <= window_up)]
    window_cont_mean = np.mean(f_cont)
    wc = []
    for _ in w_cont:
        wc.append(window_cont_mean)
    window_continuum_mean_arr = np.array([w_cont, wc])
    window_cont_list.append(window_continuum_mean_arr)
    all_local_err_fit = []
    # Approximate the error in the continuum fitting by number of points in window
    local_err_fit = 1 / ((len(f_win))**0.5)
    all_local_err_fit.append(local_err_fit)
    # Following windows
    end_loop = False
    while end_loop == False:
        window_lo = window_up
        wup_increment = window_up + window
        # Make sure that the last window is not tiny
        dist2end = max(polynomialfit_arr[0]) - window_up
        #print ('dist2end', dist2end)
        if (wup_increment < max(polynomialfit_arr[0])) and (dist2end >= window+100.0):
            window_up, _ = find_nearest(polynomialfit_arr[0], wup_increment)
        else:
            end_loop = True
            window_up = max(polynomialfit_arr[0])
        #print ('Window: ', window_lo, window_up)
        w_win = polynomialfit_arr[0][(polynomialfit_arr[0] > window_lo) & (polynomialfit_arr[0] <= window_up)]
        f_win = polynomialfit_arr[1][(polynomialfit_arr[0] > window_lo) & (polynomialfit_arr[0] <= window_up)]
        pol_wavs_lists.append(w_win)
        pol_flxs_lists.append(f_win)
        # Now the window median of the continuum
        w_cont = continuum_arr[0][(continuum_arr[0] > window_lo) & (continuum_arr[0] <= window_up)]
        f_cont = continuum_arr[1][(continuum_arr[0] > window_lo) & (continuum_arr[0] <= window_up)]
        window_cont_mean = np.mean(f_cont)
        wc = []
        for _ in w_cont:
            wc.append(window_cont_mean)
        window_continuum_mean_arr = np.array([w_cont, wc])
        window_cont_list.append(window_continuum_mean_arr)
        # Approximate the error in the continuum fitting by number of points in window
        local_err_fit = 1 / ((len(f_win))**0.5)
        all_local_err_fit.append(local_err_fit)
    err_fit = 1 / sum(all_local_err_fit)
    chi2_list = []
    for f_pol, cont in zip(pol_flxs_lists, window_cont_list):
        degreees_freedom = float(len(f_pol)) - 1.0
        # Divide the polynomial array into the same binning as the windows
        chi_sq = find_chi2(f_pol, cont[1])
        #print ('degreees_freedom = ', degreees_freedom)
        reduced_chi_sq = chi_sq / degreees_freedom
        chi2_list.append(reduced_chi_sq)
    total_chi2 = sum(chi2_list) / float(len(chi2_list))
    return total_chi2, chi2_list, window_cont_list, err_fit


def fit_polynomial(arr, order):
    '''
    This function fits a polynomial to the given array.
    Args:
        arr = 2D array of wavelengths and fluxes
        order = float, polynomial order
    Returns:
        the 2D array of the fitted polynomial, i.e wavs and flux.
    '''
    # Polynolial of the form y = Ax^5 + Bx^4 + Cx^3 + Dx^2 + Ex + F
    coefficients = np.polyfit(arr[0], arr[1], order)
    polynomial = np.poly1d(coefficients)
    f_pol = polynomial(arr[0])
    fitted_poly = np.array([arr[0], f_pol])
    return fitted_poly


def fit_continuum(object_name, object_spectra, sigmas_away=3.0, window=150, order=5, plot=True, z=None,
                  z_correct=False, normalize=True, divide_by_continuum=False,
                  lastangstroms2omit=None, firstangstroms2omit=None, wins2omit=None):
    '''
    This function fits a polunomial to the continuum. To do this the function first shifts the object's data 
    to the rest frame (z=0). The function then fits a continuum to the entire spectrum, omitting the lines in 
    the windows (using the mode flux in those regions). It then CAN normalize the entire spectrum.
    The lines it looks for are those in the lines2fit.txt file.
    Args:
        object_name = string, name of the object (will be used for output file naming)
        object_spectra = a 2D numpy array of wavelengths and fluxes
        sigmas_away = float, how many sigmas from the fode flux to use to mask emission lines to fit continuum
        window = integer, number of angstroms to consider for finding the continuum
        order = integer, the order of the polynomial, default is 5
        plot = boolean, show the plot of the fitted continuum or not
        z = float, the redshift of the object
        z_correct = boolean, bring spectrum to rest frame or not
        normalize = boolean, either divide by the continuum or subtract the continuum
        divide_by_continuum = boolean, if True flux will be divided over continuum and if False continuum
                            will be subtracted from flux (this switch will only work if normalize is set to True)
        lastangstroms2omit = float, number of angstroms to omit at the red end of the spectrum
        firstangstroms2omit = float, number of angstroms to omit at the blue end of the spectrum
        wins2omit = list of lists, each list is expected to have 2 elements, the min and max wavelength
                    to omit. This is used when there is a very prominent line (e.g. 5007 or 6563) that is pulling
                    the continuum up. The wavelengths are expected to be z-corrected. 
                    Example: wins2omit=[[4567.0, 5161.0], [6338., 6840.]]
    Returns:
        2D numpy array of continuum, it contains the wavenegths and fluxes.
        2D numpy array of redshift-corrected wavenegths and fluxes, if z_correct = True.
        '''
    print ('Calculating continuum...')
    # Bring the object to rest wavelength frame using 1+z = lambda_obs/lambda_theo - 1
    if z_correct == True:
        print ('    *** Wavelengths corrected for redshift.')
        w_corr = object_spectra[0] / (1+float(z))
    else:
        print ('    *** Wavelengths NOT YET corrected for redshift...')
        w_corr = object_spectra[0]
    # this is the array to find the continuum with
    corr_wf = np.array([w_corr, object_spectra[1]])
    wf, std_arr, avg_arr = get_spec_sigclip(corr_wf, window, sigmas_away)
    # Do you want to NOT take into account the first 150 angstroms and use the next window as average flux
    # but only if the window is small enough
    if window < 150.0:
        for i in range(len(avg_arr[1])):
            wl, idx = find_nearest(avg_arr[0], avg_arr[0][0]+150.0)
            avgfluxfirst = avg_arr[1][idx]
            if avg_arr[0][i] < wl:
                avg_arr[1][i] = avgfluxfirst150
            print (avg_arr[0][i], avg_arr[1][i])
    if firstangstroms2omit != None:
        for i in range(len(avg_arr[1])):
            # find wavelength nearest to firstangstroms2omit
            wl, idx = find_nearest(avg_arr[0], avg_arr[0][0]+firstangstroms2omit)
            avgfluxfirst = avg_arr[1][idx]
            if avg_arr[0][i] < wl:
                avg_arr[1][i] = avgfluxfirst
            print (avg_arr[0][i], avg_arr[1][i])
    if lastangstroms2omit != None:
        for i in range(len(avg_arr[1])):
            # find wavelength nearest to last wav - lastangstroms2omit
            wl, idx = find_nearest(avg_arr[0], avg_arr[0][-1]-lastangstroms2omit)
            # find closest flux to 0 in a small array around last wav
            avgfluxlast = avg_arr[1][idx]
            small_arr_f = avg_arr[1][(avg_arr[0] >= avgfluxlast-10)&(avg_arr[0] >= avgfluxlast+10)]
            flu, idx = find_nearest(small_arr_f, 0.0)
            if avg_arr[0][i] > wl:
                avg_arr[1][i] = flu
            print (avg_arr[0][i], avg_arr[1][i])
    if wins2omit != None:
        # angstroms2omit is expected to be a list of wavelength ranges to omit
        for w2o in wins2omit:
            # find wavelength nearest to beginning and end and replace flux for average flux from before and after
            wl0, idx0 = find_nearest(avg_arr[0], w2o[0])
            wl1, idx1 = find_nearest(avg_arr[0], w2o[1])
            avgfluxfirst = (avg_arr[1][idx0] + avg_arr[1][idx1] ) / 2.0
            for i in range(len(avg_arr[1])):
                wav = avg_arr[0][i]
                if wav >= wl0 and wav <= wl1:
                    avg_arr[1][i] = avgfluxfirst
                print (avg_arr[0][i], avg_arr[1][i])
    # Obtain a line from 2 continuum points:  force_continuum = [x1, y1, x2, y2]
    force_continuum = None#[4363.21, 1.321e-17, 5006.84, 1.249e-17]
    if force_continuum is not None:
        x1, y1, x2, y2 = force_continuum
        m = (y2 - y1) / (x2 - x1)
        avg_arr[1] = m*(avg_arr[0] - x1) + y1
    # Fit polynomial of the given order to all the windows
    fitted_continuum = fit_polynomial(avg_arr, order)
    total_chi2, chi2_list, window_cont_list, err_fit = find_reduced_chi2_of_polynomial(fitted_continuum, wf, window)
    print ('total_chi2, chi2_list')
    print (total_chi2, chi2_list)
    print ('percentage of error of continuum fit = %0.2f' % err_fit)
    poly_order = order
    '''
    ###### USE THIS PART WHEN WANTING TO COMPARE WITH THE FLUX-MODE CLIPPING
    ### Alternatively, use the flux mode to clip
    mode_wf = clip_flux_using_modes(corr_wf, window, threshold_fraction=2.0)
    fitted_continuum_mode = fit_polynomial(mode_wf, order)
    #print ('object_spectra[1][0], wf[1][0], fitted_continuum_mode[1][0]', object_spectra[1][0], wf[1][0], fitted_continuum_mode[1][0])
    '''
    # Plot if asked to
    if plot == True:
        plt.title(object_name)
        plt.suptitle('z-corrected spectra - polynomial of order = %s' % repr(poly_order))
        plt.xlabel('Wavelength [$\AA$]')
        plt.ylabel('Flux [ergs/s/cm$^2$/$\AA$]')
        plt.plot(corr_wf[0], corr_wf[1], 'k', fitted_continuum[0], fitted_continuum[1], 'r')
        #plt.plot(wf[0], wf[1], 'b')    # trimed flux used to fit continuum
        #plt.plot(std_arr[0], std_arr[1], 'k--')    # average standard deviations
        plt.plot(avg_arr[0], avg_arr[1], 'y')    # average of trimed flux(blue)
        if order != None:
            #plt.plot(mode_wf[0], mode_wf[1], 'g--', fitted_continuum_mode[0],fitted_continuum_mode[1],'c--')
            for cont_arr in window_cont_list:
                plt.plot(cont_arr[0], cont_arr[1], 'y')
        plt.show()
        # Normalize to that continuum if norm=True
        print ('Continuum calculated. Normalization to continuum was set to: ', normalize)
    if (normalize == True) and (plot == True):
        #before_norm = []
        norm_flux = np.array([])
        for i in range(len(corr_wf[1])):
            if divide_by_continuum:
                nf = np.abs(corr_wf[1][i]) / np.abs(fitted_continuum[1][i])
            else:
                nf = np.abs(corr_wf[1][i]) - np.abs(fitted_continuum[1][i])
            if corr_wf[1][i] < 0.0:
                nf = -1 * nf
            print (corr_wf[0][i], 'flux=', corr_wf[1][i], '   cont=',fitted_continuum[1][i], '   norm=', nf)
            norm_flux = np.append(norm_flux, nf)
            #f = nf * np.abs(fitted_continuum[1][i])   # back to non-normalized fluxes
            #before_norm.append(f)
        norm_wf = np.array([wf[0], norm_flux])
        # Give the theoretical continuum for the line finding, use scaling_factor=1.0 for division
        if divide_by_continuum:
            scale_factor = 1.0
        else:
            scale_factor = 0.0
        norm_continuum = theo_cont(corr_wf[0], scale_factor=scale_factor)
        plt.title(object_name)
        plt.suptitle('z-corrected spectra')
        plt.xlabel('Wavelength [$\AA$]')
        plt.ylabel('Normalized Flux')
        plt.plot(norm_wf[0], norm_wf[1], 'b', norm_continuum[0], norm_continuum[1], 'r')
        plt.show()
        #plt.plot(norm_wf[0], before_norm, 'g')
        #plt.show()
        return norm_wf, norm_continuum, err_fit
    else:
        return corr_wf, fitted_continuum, err_fit


def readlines_from_lineinfo(text_file, cols_in_file=None):
    """
    This function reads the information from the line catalogue.
    Args:
        text_file =  string, name of the catalogue
        cols_in_file = list of the emtpy lists to be populated
    Returns:
        cols_in_file = list of filled columns in file
    """
    if cols_in_file == None:
        catalog_wavelength = []
        observed_wavelength = []
        element = []
        ion =[]
        forbidden = []
        how_forbidden = []
        width = []
        flux = []
        continuum = []
        EW = []
        cols_in_file = [catalog_wavelength, observed_wavelength, element, ion, forbidden, how_forbidden, width, flux, continuum, EW]
    # List of the data contained in the file
    if type(text_file) is list:
        for each_file in text_file:
            f = open(each_file, 'r')
            list_rows_of_file = f.readlines()
            f.close()
    else:
        f = open(text_file, 'r')
        list_rows_of_file = f.readlines()
        f.close()
    widths_faintObj = []
    widths_strongObj = []
    for row in list_rows_of_file:
        # Disregard comment symbol
        if '#'  not in row:
            # Split each row into columns
            line_data = row.split()
            # append the element into each column in the cols_in_file
            for item, col in zip(line_data, cols_in_file):
                if '.' in item:
                    item = float(item)
                col.append(item)
        if 'widths' in row:
            kk = string.split(row, '=')
            if 'faint' in kk[0]:
                kk2 = string.split(kk[1], sep=',')
                for number in kk2:
                    widths_faintObj.append(float(number))
            elif 'strong' in kk[0]:
                kk2 = string.split(kk[1], sep=',')
                for number in kk2:
                    widths_strongObj.append(float(number))
    if len(widths_faintObj) > 0:
        return cols_in_file, widths_faintObj, widths_strongObj
    else:
        return cols_in_file


def n4airvac_conversion(wav):
    '''This function finds the index of refraction for that wavelength.
        *** Took the equation from IAU convention for wavelength conversion described in
        Morton (1991, ApJS, 77, 119)'''
    sigma_wav = 10000.0/wav
    n = 1 + 6.4328e-5 + 2.94981e-2/(146*sigma_wav*sigma_wav) + 2.5540e-4/(41*sigma_wav*sigma_wav)
    return n


def get_flux_cont_errs(object_spectra, continuum, err_instrument, err_continuum):
    # Determine the error from the instrument and the continuum fit
    err_fluxes = []
    err_contfl = []
    for f, c in zip(object_spectra[1], continuum[1]):
        ferr = f * err_instrument
        err_fluxes.append(ferr)
        cerr = c * err_continuum
        err_contfl.append(cerr)
    return err_fluxes, err_contfl


def midpoint(p1, p2):
    return p1 + (p2-p1)/2


def fit_line(arrx, arry):
    '''
    This function fits a line to the given array.
    arrx, arry = numpy arrays
    RETURNS:
        - constants of the line
        - fitted line
    '''
    order = 1
    polyinfo = np.polyfit(arrx, arry, order, full=True)
    #print ('polyinfo: ', polyinfo)
    # polyinfo = coefficients array, residuals, rank, singular values array, and conditioning threshold
    coefficients = polyinfo[0]   # these are m=coefficients[0], b=coefficients[1]
    polynomial = np.poly1d(coefficients)
    f_pol = polynomial(arrx)
    #fitted_line = np.array([arry, f_pol])
    #print 'this is x and y of the fitted_line = ', fitted_line
    # Errors -- taken from http://mathworld.wolfram.com/LeastSquaresFitting.html
    avgx = sum(arrx)/float(len(arrx))
    avgy = sum(arry)/float(len(arry))
    ssx, ssy, ssxy = [], [], []
    for xi, yi in zip(arrx, arry):
        ssxi = (xi- avgx)**2
        ssyi = (yi- avgy)**2
        ssxyi = (xi - avgx)*(yi - avgy)
        ssx.append(ssxi)
        ssy.append(ssyi)
        ssxy.append(ssxyi)
    ssx = sum(ssx)
    ssy = sum(ssy)
    ssxy = sum(ssxy)
    n = float(len(arrx))
    s = np.sqrt((ssy - (ssxy**2 / ssx))/(n-2.0))
    b_err = s * np.sqrt(1.0/n + avgx**2/ssx)
    m_err = s / np.sqrt(ssx)
    errs = [m_err, b_err]
    return coefficients, errs, f_pol


def recenter(line_wave, line_flux, original_width):
    sq_fluxes = []
    for lf in line_flux:
        sq_fluxes.append(lf*lf)
    line_peak = max(sq_fluxes)
    idx_line_peak = sq_fluxes.index(line_peak)
    recentered = line_wave[idx_line_peak]
    lower = recentered - (original_width/2.0)
    upper = recentered + (original_width/2.0)
    return recentered, lower, upper


def find_line_center_and_limits(data_arr, cont_arr, line_looked_for, lo_lim, up_lim):
    '''
    This function recenters the line according to the max or min (emission or absorption) and then adjusts
    according to the min difference between the flux and the continuum.
    Args:
        line_looked_for = the target line that we want to measure
        low = closest point in the wavelength array to lower part of the predefined width of the line
        upp = closest point in the wavelength array to upper part of the predefined width of the line
    Returns:
        recenter = float
        new_lolim = float
        new_uplim = float
    '''
    lo_lim, up_lim = float(lo_lim), float(up_lim)
    original_width = up_lim - lo_lim
    original_center = (up_lim + lo_lim)/2.0
    # unfold data and get line array
    wave, flux = data_arr
    line_wave = wave[(wave >= lo_lim) & (wave <= up_lim)]
    line_flux = flux[(wave >= lo_lim) & (wave <= up_lim)]
    # Recenter the line according to the max in the sqared fluxes
    nearest2target_line, _ = find_nearest(line_wave, line_looked_for)
    print ('Nearest wavelegth in line array to the target line =', nearest2target_line)
    new_center, new_lolim, new_uplim = recenter(line_wave, line_flux, original_width)
    print ('original_center=', original_center, '   recenter=', new_center)
    return new_center, new_lolim, new_uplim



def calculate_total_flux_and_EW(data_arr, wav_min, wav_max, cont_arr, do_errs=False):
    """
    This function calculates the equivalent widh splot style, as well as the total flux.
    Args:
        data_arr = 2d numpy array of wavelength and flux
        wav_min = float, lower limit to calculate EW
        wav_max = float, upper limit to calculate EW 
        cont_arr = 2d numpy array of continuum wavelengths and corresponding flux
        do_errs = boolean, cuurently not used - to be implemented
    Returns:
        F = float, total or summed flux
        C = float, midpoint continuum
        EW = float, measured equivalent with
    """
    # unfold data
    wave, flux = data_arr
    # create line arrays from data
    lo_wlim, _ = find_nearest(wave, wav_min)
    line_wav = wave[(wave >= wav_min) & (wave <= wav_max)]
    line_flux = flux[(wave >= wav_min) & (wave <= wav_max)]
    # start the wavelength array exactly at wav_min and make sure
    # that the line array is not empty and has at least N points
    N = 20
    if np.abs(lo_wlim-wav_min) > 0.3  or len(line_wav) < N:
        line_wav, line_flux = np.array([]), np.array([])
        delta_lambda = (wav_max - wav_min)/N
        lf = np.interp(wav_min, wave, flux)
        # include first points
        line_flux = np.append(line_flux, lf)
        line_wav = np.append(line_wav, wav_min)
        next_w = wav_min+delta_lambda
        # intermediate points
        for _ in range(N-2):
            lf = np.interp(next_w, wave, flux)
            line_flux = np.append(line_flux, lf)
            line_wav = np.append(line_wav, next_w)
            next_w = next_w+delta_lambda
        # make sure last points are at wave_max
        lf = np.interp(wav_max, wave, flux)
        line_flux = np.append(line_flux, lf)
        line_wav = np.append(line_wav, wav_max)
    # create line continuum array
    cont_wav, conf_flx = cont_arr
    # make sure the line array and the line continuum array have the same length and use same wave
    line_contflx = []
    for w in line_wav:
        lcf = np.interp(w, cont_wav, conf_flx)
        line_contflx.append(lcf)
    line_contflx = np.array(line_contflx)
    # Calculate equivalent width (splot-like) following convention of negative=emission and positive=absorption
    esum = 0.0
    wpc = np.abs((line_wav[-1] - line_wav[0]) / float(len(line_wav) - 1))
    for f, fc in zip(line_flux, line_contflx):
        esum = esum + (1-f/fc)
    esum = esum + (1.0 - line_flux[0] / line_contflx[0]) + (1.0 - line_flux[-1] / line_contflx[-1])
    EW = esum * wpc
    print ('* limits for EW calculation: ', line_wav[0], line_wav[-1])
    # Determine Flux quick and dirty
    C = midpoint(line_contflx[0], line_contflx[-1])
    F = C * EW * -1.0
    return F, C, EW


def find_lines_info(object_spectra, continuum, Halpha_width, vacuum=False, faintObj=False, text_table=False, output_file_name=None, do_errs=None):
    '''
    This function takes the object and continuum arrays to find the
    lines given in the lines_catalog.txt file.
    *** WARNING:
        This function assumes that object_spectra has already been
        corrected for redshift.
    Args:
        object_spectra = 2D numpy array of wavelengths and fluxes
        continuum = 2D numpy array of wavelengths and fluxes
        Halpha_width = float, width of the H-alpha line
        vacuum = boolean, air vacuum=False or vacuum wavelengths vacuum=True
        faintObj = boolean, this will determine the width of the lines
        text_table = boolean, write text file or not
        output_file_name = string, name of the output file
    Returns:
        catalog_wavs_found = the list of lines it found from the catalog.
        central_wavelength_list = the list of lines it found in the object spectra
        width_list = the list of widths of the lines
        net_fluxes_list = the sum of the fluxes over the line width
        continuum_list = the average continuum value over the line width
        EWs_list = the list of EWs it calculated
        if text_table=True a text file containing all this information
    '''
    # Read the line_catalog file
    line_catalog_path = 'lines_catalog.txt'
    # Define the columns of the file
    wavelength, element, ion, forbidden, how_forbidden, transition, strong_line = [], [], [], [], [], [], []
    cols_in_file = [wavelength, element, ion, forbidden, how_forbidden, transition, strong_line]
    # Define the list of the files to be read
    text_file_list = [line_catalog_path]
    # Read the files
    data, widths_faintObj, widths_strongObj = readlines_from_lineinfo(text_file_list, cols_in_file)
    wavelength, element, ion, forbidden, how_forbidden, transition, strong_line = data
    # If the wavelength is grater than 2000 correct the theoretical air wavelengths to vacuum using the IAU
    # standard for conversion from air to vacuum wavelengths is given in Morton (1991, ApJS, 77, 119). To
    # correct find the refraction index for that wavelength and then use:
    #       wav_vac / wav_air -1 = n - 1
    # (To start I usded NIST, n=0.999271)
    wavs_air, wavs_vacuum = [], []
    for w in wavelength:
        # separate air and vacuum wavelengths into 2 lists
        if w < 2000.0:
            # For keeping all vacuum wavelengths
            wavs_vacuum.append(w)
            # For converting vaccuum to air
            #print ('Wavelength < 2000, converting to air')
            wav_refraction_index = n4airvac_conversion(w)
            #print ('Refraction index  n = %f' % (wav_refraction_index))
            wair = w / (2.0 - wav_refraction_index)
            wavs_air.append(wair)
        elif w >= 2000.0:
            # For converting to vacuum wavelengths
            wav_refraction_index = n4airvac_conversion(w)
            wvac = w * (2.0 - wav_refraction_index)
            wavs_vacuum.append(wvac)
            # For keeping all AIR wavelengths
            #print ('Wavelength > 2000, keeping air')
            wavs_air.append(w)

    # Determine the strength of the lines
    width = []
    for sline in strong_line:
        if faintObj:
            widths_list = widths_faintObj
        else:
            widths_list = widths_strongObj
        if sline == "nw":
            s = widths_list[0]
        elif sline == "no":
            s = widths_list[1]
        elif sline == "weak":
            s = widths_list[2]
        elif sline == "medium":
            s = widths_list[3]
        elif sline == "yes":
            s = widths_list[4]
        elif sline == "super":
            s = widths_list[5]
        elif sline == "Halpha":
            s = Halpha_width
        width.append(s)
    # Search in the object given for the lines in the lines_catalog
    lines_catalog = (wavs_air, wavs_vacuum, element, ion, forbidden, how_forbidden, transition, width)
    net_fluxes_list, EWs_list, central_wavelength_list, catalog_wavs_found = [], [], [], []
    continuum_list, width_list = [], []
    found_element, found_ion, found_ion_forbidden, found_ion_how_forbidden = [], [], [], []
    errs_net_fluxes, errs_ews = [], []
    # but choose the right wavelength column
    if vacuum == True:
        use_wavs = 1
        use_wavs_text = '# Used VACUUM wavelengths to find lines from line_catalog.txt'
    if vacuum == False:
        use_wavs = 0
        use_wavs_text = '# Used AIR wavelengths to find lines from line_catalog.txt'
    print ('vacuum was set to %s, %s' % (vacuum, use_wavs_text))
    if do_errs != None:
        err_instrument, err_continuum = do_errs
        perc_err_continuum = err_continuum*100.0
        err_lists = get_flux_cont_errs(object_spectra, continuum, err_instrument, err_continuum)
        # err_lists contains err_fluxes and err_contfl
    for i in range(len(lines_catalog[0])):
        # find the line in the catalog that is closest to a
        line_looked_for = lines_catalog[use_wavs][i]
        nearest2line = find_nearest_within(object_spectra[0], line_looked_for, 10.0)
        if nearest2line > 0.0:
            catalog_wavs_found.append(line_looked_for)
            # If the line is in the object spectra, measure the intensity and equivalent width
            # according to the strength of the line
            central_wavelength = object_spectra[0][(object_spectra[0] == nearest2line)]
            line_width = lines_catalog[7][i]
            round_line_looked_for = np.round(line_looked_for, decimals=0)
            new_center, new_lolim, new_uplim = find_line_center_and_limits(data_arr, cont_arr, line_looked_for, lo_lim, up_lim)
            F, C, ew = calculate_total_flux_and_EW(data_arr, new_lolim, new_uplim, cont_arr, do_errs=False)
            final_width = float(new_uplim - new_lolim)
            final_width = np.round(final_width, decimals=1)
            central_wavelength = float((uplim+lolim)/2.0)
            print ('\n Looking for ',  round_line_looked_for )
            print ('This is the closest wavelength in the data to the target line: ', nearest2line)
            print ('initial center=', central_wavelength,'  initial_width=',line_width, '  final_width = %f' % final_width, '    ew=', ew)
            print ('final center=', new_center,'  Flux=',F, '  ew=', ew, '  from ', lolim, '  to ', uplim)
            width_list.append(final_width)
            central_wavelength_list.append(central_wavelength)
            continuum_list.append(C)
            net_fluxes_list.append(F)
            EWs_list.append(ew)
            found_element.append(lines_catalog[2][i])
            found_ion.append(lines_catalog[3][i])
            found_ion_forbidden.append(lines_catalog[4][i])
            found_ion_how_forbidden.append(lines_catalog[5][i])
    # Create the table of Net Fluxes and EQWs
    if linesinfo_file_name != None:
        if text_table == True:
            #linesinfo_file_name = raw_input('Please type name of the .txt file containing the line info. Use the full path.')
            txt_file = open(linesinfo_file_name, 'w+')
            print >> txt_file,  use_wavs_text
            print >> txt_file,   '# Positive EW = emission        Negative EW = absorption'
            if do_errs != None:
                print >> txt_file,   '# Percentage Error of Continuum Fit = %0.2f' % perc_err_continuum
            else:
                print >> txt_file,   '#'
            print >> txt_file,  ('{:<12} {:<12} {:>12} {:<12} {:<12} {:<12} {:<12} {:>16} {:>16} {:>12}'.format('# Catalog WL', 'Observed WL', 'Element', 'Ion', 'Forbidden', 'How much', 'Width[A]', 'Flux [cgs]', 'Continuum [cgs]', 'EW [A]'))
            for cw, w, e, i, fd, h, s, F, C, ew in zip(catalog_wavs_found, central_wavelength_list, found_element, found_ion, found_ion_forbidden, found_ion_how_forbidden, width_list, net_fluxes_list, continuum_list, EWs_list):
                #print ('cw:',type(cw), 'w:',type(w), 'e:',type(e), 'i:',type(i), 'fd:',type(fd), 'h:',type(h), 's:',type(s), 'F:',type(F), 'C:',type(C), 'ew:',type(ew) )
                print >> txt_file,  ('{:<12.3f} {:<12.3f} {:>12} {:<12} {:<12} {:<12} {:<10.2f} {:>16.3e} {:>16.3e} {:>12.3f}'.format(cw, w, e, i, fd, h, s, F, C, ew))
            txt_file.close()
            print ('File   %s   writen!' % linesinfo_file_name)
        elif text_table == False:
            print ('# Positive EW = emission        Negative EW = absorption')
            print ('{:<12} {:<12} {:>12} {:<12} {:<12} {:<12} {:<12} {:>16} {:>16} {:>12}'.format('# Catalog WL', 'Observed WL', 'Element', 'Ion', 'Forbidden', 'How much', 'Width[A]', 'Flux [cgs]', 'Continuum [cgs]', 'EW [A]'))
            for cw, w, e, i, fd, h, s, F, C, ew in zip(catalog_wavs_found, central_wavelength_list, found_element, found_ion, found_ion_forbidden, found_ion_how_forbidden, width_list, net_fluxes_list, continuum_list, EWs_list):
                print ('{:<12.3f} {:<12.3f} {:>12} {:<12} {:<12} {:<12} {:<12} {:>16.3e} {:>16.3e} {:>12.3f}'.format(cw, w, e, i, fd, h, s, F, C, ew))
        if do_errs != None:
            return catalog_wavs_found, central_wavelength_list, width_list, net_fluxes_list, continuum_list, EWs_list, errs_net_fluxes, errs_ews
        else:
            return catalog_wavs_found, central_wavelength_list, width_list, net_fluxes_list, continuum_list, EWs_list
    else:
        if do_errs != None:
            return catalog_wavs_found, central_wavelength_list, width_list, net_fluxes_list, continuum_list, EWs_list, errs_net_fluxes, errs_ews
        else:
            return catalog_wavs_found, central_wavelength_list, width_list, net_fluxes_list, continuum_list, EWs_list



